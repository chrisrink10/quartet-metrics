# Quartet Health Metrics Server

__Author:__ Chris Rink

The Quartet Health metric server is written in Python (version 3) using 
Flask as the framework for serving HTTP requests and SQLite as the underlying
data store.

## Getting Started

To install and run the metrics server instance, use the following commands
from a UNIX-like operating system. You will want to select a location to
install the server -- for testing purposes a folder in your user home 
directory should suffice.

    # Create a folder and a Python virtualenv
    mkdir quartet-metrics
    cd quartet-metrics
    python3 -m venv venv
    source venv/bin/activate
    
    # Upgrade pip and install quartet-metrics
    pip install --upgrade pip
    pip install git+ssh://git@bitbucket.org/chrisrink10/quartet-metrics.git
    
    # Run the quartet-metrics tests
    qmetrics test
    
    # Setup quartet-metrics database and config
    qmetrics setup -c -d ./_metrics.db
    
    # Run the (development) server at 127.0.0.1:5000
    qmetrics run
    
You can use `cURL` to submit requests to the server to test the service:

    quartet-metrics$ curl -H "Content-Type: application/json" -X POST -d '{"name":"some_metric","timestamp":"20160424T151603Z","value":"87"}' http://127.0.0.1:5000/api/metrics
    quartet-metrics$ curl -H "Accept: application/json" -H "Content-Type: application/json" -X GET http://127.0.0.1:5000/api/aggregates/some_metric
    87    
    
To run the Quartet Metrics server in a production setting, you would need
to get `nginx` and a WSGI gateway app such as `uWSGI`. `nginx` would accept
the frontend HTTP requests and pass them along to `uWSGI`. `uWSGI` would
manage several instances of the Python/Flask app and act as a load balancer
for those processes. 

## API

Clients communicate with the Quartet Health metrics server using an HTTP/1.1
interface as described below. 

### Versions

I have elected to represent the version number of the API in the URI. This
is far from a community consensus for "proper" versioning techniques of HTTP
APIs. Other common options include versioning the "Accept" HTTP header MIME 
type or versioning a custom request header. 

I selected to version the API via the URI because it is the lowest friction
implementation strategy for both the API maintainer and API consumers. This 
strategy is employed by other popular HTTP APIs such as Twitter and Facebook, 
so it is likely to be familiar to other developers despite any arguments
against this method.

The most common argument I could find against including the version as part of
the URI is that the URI should represent an _entity_ on the server and 
including a version number does not make semantic sense; that is, you are 
not requesting the version of an entity, but rather the entity itself [1]. 
Each of the other techniques I mentioned above come with some downsides as
well, so I merely selected what I believe to be the best tradeoff between
semantic correctness and usability.

Quartet Health Metric Server API consumers may either elect for the latest
API (by excluding the API version number from the request URI) or a specific
API version. This should allow the API maintainers to phase in API and 
interface changes without introducing breaking changes to critical clients.
This also means that _some_ percentage of clients can be upgraded while 
others are left on the existing APIs to test platform changes on subsets 
of the client population.

### Naming

Naming HTTP resources -- specifically pluralization of those names -- is a 
another somewhat contentious topic. I have elected to pluralize all API names
even in the case of operations which do not specifically deal with collections
of resources for the sake of simplicity. This approach does sacrifice some
semantic meaning attached to an API request. However, I believe the virtues
of a consistent API name scheme outweigh any minor loss in semantic meaning
incurred from such a scheme. 

In particular, it is entirely possible that a once-singular resource could 
eventually offer a collection-based endpoint, at which point that API endpoint 
would be inconsistent with the rest of the resource endpoints in the API in 
being singular merely "for historical reasons". 

### Metric Entity

Metric entities are generic containers consisting of a name, value, and
a timestamp that the metric was collected. Note that the timestamp should 
be in a human readable format. An example metric entity in JSON format is 
given below:

    {
        "name": "active_sessions",
        "value": 253,
        "timestamp": "Mon Apr 18 20:15:11 +0000 2016"
    }

### Get All Metrics

`GET /api/metrics`

`GET /api/1.0/metrics`

Request a list of all metrics reported to the metric server. 

The response will be a JSON array of all metric entities reported up to 
the server.

### Get Individual Metric

`GET /api/metrics/{metric}?time={start}`

`GET /api/1.0/metrics/{metric}?time={start}`

Request the value of a metric at a specific time. 

The response will be a JSON array of the metric observations for the requested
metric submitted at the requested time. Note that this is always an array
because the metric server does not enforce a uniqueness constraint on 
metric/timestamp tuples. 

### Get Aggregate Of One Metric

`GET /api/aggregates/{metric}?start={start}&end={end}`

`GET /api/1.0/aggregates/{metric}?start={start}&end={end}`

Request an aggregate of values of a given metric within a specified
time range. 

The response will be a JSON number of the aggregated value of the metric
over the given time range.

### Submit A Metric

`POST /api/metrics`

`POST /api/1.0/metrics`

Submit an single metric to the server as a JSON object. Metrics should include
a _name_, _timestamp_, and _value_ attribute as described above. 

## Approach

I have elected to construct the Quartet Health metrics server using an 
HTTP/1.1 interface, which is documented above. HTTP APIs are a natural
choice for service level APIs which may need to speak to a wide range of
clients. HTTP client and server libraries exist for nearly every language
and platform on the planet and at this point a huge volume of applications
make HTTP requests at some point in their lifetime. This means that it is 
extremely likely that other developers will be familiar with the interface
and can be brought up to speed quickly on how to use the API. 

Another ancillary benefit of HTTP communication is that it is tremendously
easy to debug since it is entirely text based. Binary protocols require 
specialized tools to read and create the data, so this can slow down the
development process and mask obscure bugs. 

Note that for extremely high-volume, performance-critical servers, the 
benefit of choosing a binary protocol might outweigh the benefits of 
programmer productivity and familiarities. As always, it would be best to
measure performance before assuming that the text protocol is necessarily
the bottleneck of the service.

###  Handling Parallel Requests

One of the basic requirements of the metric server project is that any 
submission handle requests in parallel. Flask apps are not inherently 
capable of handling parallel requests, but they are typically run behind a 
WSGI gateway software like uWSGI [2] or Gunicorn [3]. The WSGI gateway 
interface runs multiple instances of the application and acts as a gateway 
between the web server and app, balancing the request load from the web
server out to each of the app worker processes.

### Batch Requests

In my initial design pass on the API, I decided that the metric submission
API should accept an array of metric JSON objects to permit clients to submit
an arbitrary number of resources in a single HTTP request. Practically 
speaking, this presents a problem since HTTP servers are _advised_ [4] to 
return the URI for the POST-ed resource. Semantically speaking, it is 
harder to reason about errors in the POST-ed resource values.

Of course, this does present the problem for a client wishing to submit
a large quantity of metrics in a single request. This could become a 
performance issue for the server. As always with performance issues, the
recommendation is that measurements should be taken to determine where 
the performance issue exists. In this particular case, it is likely that
a higher volume of network requests (one per metric submission) could
pose a performance problem. However, it is also likely that a bigger 
bottleneck will be in the disk I/O associated with writing out to the 
database. 

In researching how to handle this problem, I found that there are 
(unsurprisingly) differing opinions on how this should be handled in the
architecture. Some people suggested allowing a collection-style bulk
endpoint, whereas others suggested that servers stick to single resource
endpoints. If it were discovered that this design choice were in fact
causing significant performance issues, the API could easily be extended
to accommodate a bulk submission API, like the one used by Facebook [5].

### Persistence

In a real production environment, an application such as this one would
_likely_ use a separate RDBMS server such as MySQL or PostgreSQL to handle 
metric persistence and queries. I selected SQLite for this particular project 
since it is easy to get set up for a relatively small project and requires 
zero maintenance or administration. 

That said, depending on the intended architecture of this system, the expected
volume of connections, the expected ratio of reads to writes, and the
expected size of the dataset, it may be perfectly acceptable to use SQLite
as the backing store. The SQLite website [6] details a number of use cases for
which SQLite is appropriate and a number of use cases for which they recommend
a dedicated RDBMS server.

This project could be trivially migrated to a different RDBMS since it does
not use any special features that depend on any specific RDBMS. To switch,
a developer would only need to acquire the correct database driver and swap
out the API calls. The SQL DDL statements to create the database will also 
likely need to be updated, since SQLite uses less specific data type names than
other databases (e.g. `TEXT` rather than `VARCHAR`).

## Links

1. [Troy Hunt on API Versioning](https://www.troyhunt.com/your-api-versioning-is-wrong-which-is/)
2. [uWSGI](https://github.com/unbit/uwsgi)
3. [Gunicorn](http://gunicorn.org)
4. [W3 HTTP Statuses](http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)
5. [Facebook Bulk Submission API](https://developers.facebook.com/docs/graph-api/making-multiple-requests)
6. [When to use SQLite](https://www.sqlite.org/whentouse.html)