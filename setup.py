try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='quartet-metrics',
    version='0.1',
    packages=['metrics'],
    py_modules=['metrics_tests'],
    url='',
    license='',
    author='Christopher Rink',
    author_email='chrisrink10@gmail.com',
    description='Quartet Health metrics server',
    install_requires=[
        'Flask>=0.10.1',
        'python-dateutil>=2.5.2'
    ],
    include_package_data=True,
    scripts=['bin/qmetrics'],
    package_data={
        'scripts': 'metrics/scripts/*',
    },
)
