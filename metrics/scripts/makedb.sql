-- Metrics DB table
--   Create a table to store the metrics and a few indexes to make the
--   queries we expect on the data to be faster as the data set grows.

-- NOTE:
--   This table uses an INTEGER type for the timestamp. This is a
--   simplification of the standard UNIX/POSIX timestamp type, which can
--   be represented as a floating point value for sub-second granularity.
CREATE TABLE IF NOT EXISTS metrics (
  name        TEXT,
  timestamp   INTEGER,
  value       TEXT
);

CREATE INDEX name ON metrics (name);
CREATE INDEX time_range ON metrics (name, timestamp);