import json


def stream_json_array(func):
    """Create a generator which can stream a JSON array of arbitrary JSON
    elements back to the client.

    Flask supports the ability to Stream arbitrarily large contents back to
    the client using generators to avoid the problem of having to buffer all
    of the response in memory at once. The relevant section of the Flask
    documentation is `<http://flask.pocoo.org/docs/0.10/patterns/streaming/>`.

    The syntax of JSON Arrays and Objects prevents using generators directly
    from the database since we need to be mindful of opening and closing
    brackets and braces and making sure not to output trailing commas. This
    method was adapted from
    `here <https://blog.al4.co.nz/2016/01/streaming-json-with-flask/>`

    :param func:
        a generator function which returns elements to be serialized to

    :returns:
        a generator function which returns a JSON array of the serialized
        elements"""
    yield '['
    elems = iter(func)
    prev = None

    try:
        prev = next(elems)
    except StopIteration:
        yield ']'
        raise

    assert prev is not None
    for elem in elems:
        yield json.dumps(prev) + ','
        prev = elem

    yield json.dumps(prev)
    yield ']'
