import dateutil.parser
import flask
import metrics.db
import metrics.util


app = flask.Flask(__name__)
app.config.from_object('metrics.settings')
app.config.from_envvar('QUARTET_METRICS_SETTINGS', silent=True)


@app.route("/api/metrics", methods=['GET'])
@app.route("/api/1.0/metrics", methods=['GET'])
def get_all_metrics():
    """Stream all metrics collected on the server back to the client.

    :returns:
        a JSON array containing every metric in the database"""
    db = app.config['DATABASE']
    stream = metrics.util.stream_json_array(metrics.db.get_all(db))
    return flask.Response(stream, mimetype="application/json")


@app.route("/api/metrics/<metric>", methods=['GET'])
@app.route("/api/1.0/metrics/<metric>", methods=['GET'])
def get_one_metric(metric):
    """Stream any entries of a metric at a specific time back to the client.

    :param metric:
        the name of the metric to match

    :arg time:
        the timestamp of the relevant metric(s)

    :returns:
        a JSON array containing all metrics with the specified
        name/timestamp combination

    :raises BadRequest:
        if the timestamp cannot be parsed

    :raises NotFound:
        if no metric entries are found for the requested time range and
        metric name"""
    time = flask.request.args['time']

    # Verify that the timestamp can be parsed
    try:
        dateutil.parser.parse(time)
    except ValueError:
        flask.abort(400)

    db = app.config['DATABASE']
    stream = metrics.util.stream_json_array(metrics.db.get(db, metric, time))
    return flask.Response(stream, mimetype="application/json")


@app.route("/api/aggregates/<metric>", methods=['GET'])
@app.route("/api/1.0/aggregates/<metric>", methods=['GET'])
def get_metric_aggregate(metric):
    """Compute the aggregate of a metric within a time range.

    :param metric:
        the name of the metric to aggregate

    :arg start:
        the human readable start timestamp of a range-specified query or
        None if the start is unspecified

    :arg end:
        the human readable end timestamp of a range-specified query or
        None if the end is unspecified

    :returns:
        a JSON number representing the aggregate of the metric over
        the specified time range

    :raises BadRequest:
        if the start or end timestamps cannot be parsed

    :raises NotFound:
        if no metric entries are found for the requested time range and
        metric name"""
    start = flask.request.args.get('start', None)
    end = flask.request.args.get('end', None)
    db = app.config['DATABASE']
    try:
        aggregate = metrics.db.get_aggregate(db, metric, start=start, end=end)
        return flask.Response(str(aggregate), mimetype="application/json")
    except KeyError:
        flask.abort(404)
    except ValueError:
        flask.abort(400)


@app.route("/api/metrics", methods=['POST'])
@app.route("/api/1.0/metrics", methods=['POST'])
def submit_metrics():
    """Save a submitted metric to the database.

    :raises BadRequest:
        if the submitted metric does not have all of the required attributes
        or if the metric timestamp cannot be parsed"""
    body = flask.request.get_json()
    db = app.config['DATABASE']

    try:
        metrics.db.set(db, body)
    except (KeyError, ValueError):
        flask.abort(400)

    # Do not allow workzeug library to autocorrect the Location header
    # to an absolute URI; this appears to be in response to RFC 2616,
    # which was made obsolete by RFC 7231. The latter RFC permits
    # a relative URI in the Location header
    resp = flask.make_response()
    resp.autocorrect_location_header = False
    resp.headers["Location"] = flask.url_for("get_one_metric",
                                             metric=body["name"],
                                             time=body["timestamp"])
    return resp


if __name__ == "__main__":
    app.run(debug=True)
