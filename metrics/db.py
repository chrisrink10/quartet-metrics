import datetime
import sqlite3
import dateutil.parser


def get_all(db):
    """Get every individual metric data point that has been collected.

    :param db:
        the filename of the database to use for this query

    :returns:
        a generator which will return all metric values stored in the
        database"""
    with sqlite3.connect(db) as conn:
        query = ("SELECT name, timestamp, value FROM metrics "
                 "ORDER BY name, timestamp")
        for row in conn.execute(query):
            yield {
                "name": row[0],
                "timestamp": _time_out(row[1]),
                "value": row[2]
            }


def get_aggregate(db, name, start=None, end=None):
    """Get the aggregate of a single metric over a given time range.

    :param db:
        the filename of the database to use for this query

    :param name:
        the name of the metric to aggregate

    :param start:
        the human readable start timestamp of a range-specified query or
        None if the start is unspecified

    :param end:
        the human readable end timestamp of a range-specified query or
        None if the end is unspecified

    :returns:
        the aggregate value of the metric over the specified time range

    :raises ValueError:
        if either the start or end date cannot be parsed

    :raises KeyError:
        if no metric entries are found for the range"""
    with sqlite3.connect(db) as conn:
        query = _aggregate_query(start=start, end=end)
        params = _aggregate_params(name, start=start, end=end)
        row = conn.execute(query, params).fetchone()
        if row[0] is None:
            raise KeyError
        return row[0]


def _aggregate_query(start=None, end=None):
    """Generate the metric aggregate SELECT SQL query.

    :param start:
        the human readable start timestamp of a range-specified query or
        None if the start is unspecified

    :param end:
        the human readable end timestamp of a range-specified query or None
        if the end is unspecified

    :returns:
        a SQL SELECT query to select metrics within the given time range"""
    return ("SELECT sum(value) FROM metrics "
            "WHERE name = ? {start} {end} ").format(
        start="" if start is None else "AND timestamp >= ?",
        end="" if end is None else "AND timestamp <= ?"
    )


def _aggregate_params(name, start=None, end=None):
    """Generate the tuple of parameters for a metric aggregate query.

    :param name:
        the name of the metric to select

    :param start:
        the human readable start timestamp of a range-specified query or
        None if the start is unspecified

    :param end:
        the human readable end timestamp of a range-specified query or
        None if the end is unspecified

    :returns:
        a tuple of the parameters to use for the query properly
        formatted for direct usage

    :raises ValueError:
        if either of the start or end dates cannot be parsed as dates
        correctly or if start is after end"""
    params = [name]
    if start is not None:
        params.append(_time_in(start))
    if end is not None:
        params.append(_time_in(end))
    if start is not None and end is not None:
        if params[1] > params[2]:
            raise ValueError
    return tuple(params)


def get(db, name, timestamp):
    """Get the value(s) of a metric at a certain time from the database.

    Note that it is not guaranteed that a single metric `(name, timestamp)`
    tuple uniquely identify one value. Timestamps are not granular enough
    to guarantee uniqueness even within individual metrics.

    :param db:
        the filename of the database to use for this query

    :param name:
        the name of the metric to select

    :param timestamp:
        the timestamp of the metric to select

    :returns:
        a generator which will return all metric values matching the given
        name and timestamp combination

    :raises ValueError:
        if the timestamp cannot be parsed"""
    with sqlite3.connect(db) as conn:
        query = ("SELECT name, timestamp, value FROM metrics "
                 "WHERE name = ? AND timestamp = ?")
        params = (name, _time_in(timestamp))
        for row in conn.execute(query, params):
            yield {
                "name": row[0],
                "timestamp": _time_out(row[1]),
                "value": row[2]
            }


def set(db, metric):
    """Insert a metric into the database.

    :param db:
        the filename of the database to use for this query

    :param metric:
        a metric entity (as a dict-like object), which has the following
        attributes:
            name        - the name of the metric
            timestamp   - a human readable timestamp to associate with
                          the metric
            value       - the value associated with the metric

    :raises KeyError:
        if the metric does not have one of the three required properties

    :raises ValueError:
        if metric timestamp cannot be parsed or if the metric has no name"""
    with sqlite3.connect(db) as conn:
        query = "INSERT INTO metrics VALUES (?, ?, ?)"
        if len(metric["name"]) == 0:
            raise ValueError
        ts = _time_in(metric["timestamp"])
        val = _val_in(metric["value"])
        params = (metric["name"], ts, val)
        conn.execute(query, params)
        # Implicit commit from using context manager


def _val_in(value):
    """Convert the value into a numeric Python value.

    :param value:
        the input value

    :returns:
        either a float or int corresponding to the input value

    :raises ValueError:
        if the value could not be converted to an integer or a floating
        point value"""
    try:
        v = int(value)
        return v
    except ValueError:
        return float(value)


def _time_in(time):
    """Convert the given human readable timestamp into an integral POSIX
    timestamp value.

    Note that the timestamp is always truncated to an integral value.
    If additional granularity is required in reporting or metric accuracy,
    then the metric column in the database should be converted to `REAL`
    (for SQLite) or whatever the corresponding floating point value is for
    the target RDBMS. At this point, timestamps are being accepted in human
    readable formats, which can specify subsecond accuracy, but often do not.

    :param time:
        a human readable time format string

    :returns:
        a POSIX timestamp truncated to an integral value

    :raises ValueError:
        when a date cannot be parsed correctly"""
    ts = dateutil.parser.parse(time)
    return int(ts.timestamp())


def _time_out(time):
    """Convert the given POSIX timestamp value into a human readable time
    format.

    :param time:
        a POSIX timestamp value

    :returns:
        a human readable timestamp format in ISO 8601 format"""
    ts = datetime.datetime.fromtimestamp(time)
    return ts.isoformat()
