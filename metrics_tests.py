import datetime
import json
import os
import pkg_resources
import sqlite3
import tempfile
import unittest
import metrics.server


class MetricsTest(unittest.TestCase):
    def setUp(self):
        self.db_fd, metrics.server.app.config['DATABASE'] = tempfile.mkstemp()
        metrics.server.app.config['TESTING'] = True
        self.client = metrics.server.app.test_client()

        # Set up the database
        with sqlite3.connect(metrics.server.app.config['DATABASE']) as conn:
            with open(self._sql_file, mode='r') as f:
                create_script = f.read()
                conn.executescript(create_script)

        # Create an application context for the app configuration
        self.ctx = metrics.server.app.app_context()
        self.ctx.push()

    def tearDown(self):
        self.ctx.pop()
        os.close(self.db_fd)
        os.unlink(metrics.server.app.config['DATABASE'])

    @property
    def _sql_file(self):
        path = os.path.join('scripts', 'makedb.sql')
        return pkg_resources.resource_filename('metrics', path)

    @property
    def _now(self):
        """Generate an ISO 8601 timestamp for the current second."""
        return self._time_format(datetime.datetime.now())

    def _now_offset(self, offset):
        """Generate an ISO 8601 timestamp for a time `offset` seconds from
        the current time.

        :param offset:
            a number of seconds to offset the returned timestamp from now;
            this value can be negative or positive

        :returns:
            an ISO 8601 formatted timestamp for the offset time"""
        t = datetime.datetime.now()
        return self._time_format(t + datetime.timedelta(seconds=offset))

    def _time_format(self, dt):
        """Create a formatted time string without microseconds.

        Since the metric server truncates seconds, this makes comparing
        timestamps more reliable between POST and GET requests.

        :param dt:
            a Python `datetime.datetime` object

        :returns:
            an ISO 8601 formatted date time string"""
        return dt.replace(microsecond=0).isoformat()

    def test_empty_db(self):
        rv = self.client.get('/api/metrics')
        self.assertEqual(rv.data, b"[]", "should have no metrics")

    def test_get_all_metrics(self):
        cases = [
            {"name": "unique_visitors", "timestamp": self._now, "value": "31415"},
            {"name": "unique_visitors", "timestamp": self._now_offset(-5), "value": "27182"},
            {"name": "unique_visitors", "timestamp": self._now_offset(-10), "value": "14142"},
        ]

        for case in cases:
            prv = self.client.post('/api/metrics',
                                   data=json.dumps(case),
                                   content_type='application/json')
            self.assertEqual(prv.status_code, 200)

        prv = self.client.get('/api/metrics')
        resp = json.loads(str(prv.data, encoding='utf8'))
        self.assertEqual(len(resp), len(cases))
        self.assertTrue(all(x in cases for x in resp))

    def test_get_one_metric(self):
        # Submit a few distinct measurements
        cases = [
            {"name": "unique_visitors", "timestamp": self._now, "value": "31415"},
            {"name": "unique_visitors", "timestamp": self._now_offset(-5), "value": "27182"},
            {"name": "unique_visitors", "timestamp": self._now_offset(-10), "value": "14142"},
        ]

        for case in cases:
            prv = self.client.post('/api/metrics',
                                   data=json.dumps(case),
                                   content_type='application/json')
            self.assertEqual(prv.status_code, 200)

        # Verify that those measurements are returned
        for case in cases:
            prv = self.client.get('/api/metrics/unique_visitors',
                                  query_string=dict(time=case["timestamp"]))
            resp = json.loads(str(prv.data, encoding='utf8'))
            self.assertEqual(len(resp), 1)
            self.assertDictEqual(case, resp[0])

        # Submit a few invalid requests
        prv = self.client.get('/api/metrics/unique_visitors',
                              query_string=dict(time="3832jkdkldskd"))
        self.assertEqual(prv.status_code, 400)

        prv = self.client.get('/api/metrics/unique_visitors')
        self.assertEqual(prv.status_code, 400)

        # Submit a duplicated value and make sure we get both values back
        newcase = {
            "name": "unique_visitors",
            "timestamp": cases[0]["timestamp"],
            "value": "31414"
        }
        prv = self.client.post('/api/metrics',
                               data=json.dumps(newcase),
                               content_type='application/json')
        self.assertEqual(prv.status_code, 200)

        prv = self.client.get('/api/metrics/unique_visitors',
                              query_string=dict(time=newcase["timestamp"]))
        resp = json.loads(str(prv.data, encoding='utf8'))
        self.assertEqual(len(resp), 2)
        self.assertTrue(all(x in [newcase, cases[0]] for x in resp))

    def test_submit(self):
        metric = {
            "name": "unique_visitors",
            "timestamp": self._now,
            "value": "345"
        }
        prv = self.client.post('/api/metrics',
                               data=json.dumps(metric),
                               content_type='application/json')
        self.assertIsNotNone(prv)
        self.assertIsNotNone(prv.headers)
        self.assertIsNotNone(prv.headers['Location'])

        grv = self.client.get(prv.headers['Location'])
        resp = json.loads(str(grv.data, encoding='utf8'))
        self.assertEqual(len(resp), 1)

        self.assertDictEqual(metric, resp[0])

    def test_submit_fail(self):
        cases = [
            {"timestamp": self._now, "value": "345"},
            {"name": "unique_visitors", "value": "345"},
            {"name": "unique_visitors", "timestamp": self._now},
            {"name": "unique_visitors", "timestamp": "322223933", "value": "345"},
            {"name": "", "timestamp": "322223933", "value": "345"},
        ]

        for case in cases:
            prv = self.client.post('/api/metrics',
                                   data=json.dumps(case),
                                   content_type='application/json')
            self.assertEqual(prv.status_code, 400)

    def test_aggregate(self):
        # Post up two metrics to the server
        post_metrics = [
            {
                "name": "unique_visitors",
                "timestamp": self._now_offset(-5),
                "value": "345"
            },
            {
                "name": "unique_visitors",
                "timestamp": self._now,
                "value": "355"
            }
        ]

        for metric in post_metrics:
            prv = self.client.post('/api/metrics',
                                   data=json.dumps(metric),
                                   content_type='application/json')
            self.assertIsNotNone(prv)
            self.assertIsNotNone(prv.headers)
            self.assertIsNotNone(prv.headers['Location'])

        # Verify that the proper aggregate is calculated
        cases = [
            {
                "query_string":dict(start=post_metrics[0]['timestamp'],
                                    end=post_metrics[1]['timestamp']),
                "resp":700,
            },
            {
                "query_string":dict(end=post_metrics[0]['timestamp']),
                "resp":345,
            },
            {
                "query_string":dict(start=post_metrics[1]['timestamp']),
                "resp":355,
            }
        ]
        for case in cases:
            grv = self.client.get('/api/aggregates/unique_visitors',
                                  query_string=case['query_string'])
            resp = json.loads(str(grv.data, encoding='utf8'))
            self.assertEqual(resp, case['resp'])

        # Verify that if start > end, an error is generated
        grv = self.client.get('/api/aggregates/unique_visitors',
                              query_string=dict(
                                  start=post_metrics[1]['timestamp'],
                                  end=post_metrics[0]['timestamp']))
        self.assertEqual(grv.status_code, 400)


if __name__ == "__main__":
    unittest.main()
